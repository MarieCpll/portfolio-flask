from flask import Flask, render_template, request, redirect, url_for
from flask_pymongo import pymongo
from bson.objectid import ObjectId


CONNECTION_STRING = "mongodb+srv://dbUser:RStiaVXznPzdNLMi@cluster0-jwlck.mongodb.net/test"
client = pymongo.MongoClient(CONNECTION_STRING)
db = client.get_database('Portfolio')
user_collection = pymongo.collection.Collection(db, 'Articles')
app = Flask(__name__)


########## ARTICLES ############

### Récupérer les données et les afficher
@app.route("/articles", methods = ['GET', 'POST'])
def articles():	
	results = db.articles.find()
	articles = []
	for liste in results:
		articles.append({'id':liste['_id'], 'title':liste['title'],'description': liste['description'],'image_url': liste['image_url']})
	return render_template("articles.html", articles=articles)

### Créer un Nouveau
@app.route("/create/article", methods=['GET', 'POST'])
def addArticle():
	if request.method == "POST":
		title = request.form.get("title")
		description = request.form.get("description")
		image = request.form.get("image")

		db.articles.insert_one({"title":title, "description": description, "image_url": image})
		return render_template('createArticle.html')
	else:
		return render_template('createArticle.html')

### Supprimer
@app.route("/delete/article", methods=['POST','GET'])
def deleteArticle():
	key = request.form['article_id']
	db.articles.remove({'_id':ObjectId(key)})
	return redirect("/articles")


@app.route('/edit', methods=['POST', 'GET'])
def update():
	if request.method == 'GET':
		key = request.form.get('article_id')
		article = db.articles.find_one({'_id': ObjectId(key)})
		return render_template("editArticle.html", article=article)
	if request.method == 'POST':
	    cle= request.form.get('article_id')
	    article = db.articles.find_one({'_id': ObjectId(cle)})
	    title= request.form['title']
	    description = request.form['description']
	    image = request.form['image']
	    key= {'_id': ObjectId(cle) }
	    
	    db.articles.update_one(article, {'$set': {'title':title, 'description': description, 'image': image}})
	    return redirect('/articles')


########## PROJETS ############

### Récupérer les données et les afficher
@app.route("/projets", methods = ['GET', 'POST'])
def projets():	
	results = db.projets.find()
	projets = []
	for liste in results:
		projets.append({'id':liste['_id'], 'title':liste['title'],'description': liste['description'],'image_url': liste['image_url'], 'technology': liste['technology'], 'repo_url': liste['repo_url'], 'website_url': liste['website_url']})
	return render_template("projets.html", projets=projets)

### Créer un Nouveau
@app.route("/create/projet", methods=['GET', 'POST'])
def addProjet():
	if request.method == "POST":
		title = request.form.get("title")
		description = request.form.get("description")
		image = request.form.get("image")
		technology = request.form.get("technology")
		repo_url = request.form.get("repo_url")
		website_url = request.form.get("website_url")

		db.projets.insert_one({"title":title, "description": description, "image_url": image, 'technology': technology, 'repo_url': repo_url, 'website_url': website_url})
		return render_template('createProjet.html')
	else:
		return render_template('createProjet.html')

### Supprimer
@app.route("/delete/projet", methods=['POST'])
def deleteProjet():
	key = request.form['projet_id']
	db.projets.remove({'_id':ObjectId(key)})
	return redirect("/projets")



if __name__ == "__main__":
	app.run(debug=True)
